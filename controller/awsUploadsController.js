import Users from '../models/model.user.js'
import aws from 'aws-sdk'
import fs from 'fs'

const signup = async (req, res) => {
  try {
    aws.config.setPromisesDependency()
    aws.config.update({
      accessKeyId: process.env.ACCESSKEYID,
      secretAccessKey: process.env.SECRETACCESSKEY,
      region: process.env.REGION,
    })
    const s3 = new aws.S3()

    const params = {
      ACL: 'public-read',
      Bucket: process.env.BUCKET_NAME,
      Body: fs.createReadStream(req.file.path),
      Key: `userAvatar/${Date.now() + req.file.originalname}`,
    }

    const data = await s3.upload(params).promise()
    if (data) {
      fs.unlinkSync(req.file.path)
      const locationURL = data.Location
      const { email, name, password } = req.body
      let user = new Users({ email, name, password, avatar: locationURL })
      await user.save()
      res.json({ msg: 'User created Succesfully', user })
    }
  } catch (error) {
    console.log(error.message)
    res.json({ err: error.message })
  }
}

export default signup
