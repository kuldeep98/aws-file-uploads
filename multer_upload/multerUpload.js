import multer from 'multer'
import path from 'path'

const storage = multer.diskStorage({
  destination: './public/uploads',
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname +
        '-' +
        new Date().getMilliseconds() +
        path.extname(file.originalname)
    )
  },
})

const myCallback = function (file, cb) {
  const filetypes = /jpeg|png|jpg|gif/
  const extname = filetypes.test(file.originalname)
  const mimetype = filetypes.test(file.mimetype)

  if (mimetype && extname) {
    cb(null, true)
  } else {
    cb('Error: Imanges Only!!!')
  }
}
const upload = multer({
  storage,
  limits: { fileSize: 1000000 },
  fileFilter: function (req, file, cb) {
    myCallback(file, cb)
  },
}).single('myImage')

export default upload
