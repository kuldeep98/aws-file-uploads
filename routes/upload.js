import express from 'express'
import multer from 'multer'
import signup from '../controller/awsUploadsController.js'
import { uploadImage } from '../controller/uploadController.js'
const router = express.Router()

router.post('/', uploadImage)

router.post('/signup',multer({
    dest:'temp/',limits:{fieldSize:8*1024*1024}
}).single('avatar'),signup)

export default router
