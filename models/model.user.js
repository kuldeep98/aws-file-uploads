import mongoose from 'mongoose'

const userSchema = new mongoose.Schema({
  email: String,
  password: String,
  name: String,
  avatar: String,
})

const Users = mongoose.model('users', userSchema)

export default Users
